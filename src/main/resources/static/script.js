
    const el = document.getElementById("submit");
    el.addEventListener("click", submit);


    async function submit() {

      nom = document.getElementById("name");
      code = document.getElementById("code");

      
      body = { name: nom.value,
        code: code.value 
      }


      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify(body);

     var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };


    fetch("/restonsApi/country", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
      
    }

    async function getCountry(){
      var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("/restonsApi/country", requestOptions)
        .then(response => response.text())
        .then(result => this.showList(result))
        .catch(error => console.log('error', error));
    }

    this.getCountry();


    function showList(data){

      const table = document.getElementById("list")

      JSON.parse(data).forEach(element => {
        list=document.createElement("tr")
        table.appendChild(list)

        let name=document.createElement("td")
        name.innerText=element.name;
        list.appendChild(name)
    
        let code=document.createElement("td")
        code.innerText=element.code;
        list.appendChild(code)
      
      
        deleteButon(element.id,list);
        uptadeButon(element.id,list)
      });


    }

    function deleteCountry(id,element){

    
      element.remove();
      var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
      };
      
      fetch("restonsApi/country/"+id, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    }

    function deleteButon(id, element){
      
    bt= document.createElement("button")
    bt.onclick = function() {deleteCountry(id, element)};
    bt.innerText="supprimer";
    element.appendChild(bt);
    }

    function uptadeButon(id, tr){
      
    const bt= document.createElement("button")
      bt.onclick = function() {showUpdata(id, tr,bt)};
      bt.innerText="modifier";
      tr.appendChild(bt);
    }


    function showUpdata(id,tr,bt){

      const mdr= tr.getElementsByTagName('td')

      for (let index = 0; index < mdr.length; index++) {
        element = mdr[index];

        const input= document.createElement("input")
        input.type='text'
        text=element.innerText
        element.innerText=""
        input.value=text
        element.appendChild(input);
      }

      bt.onclick = function() {updataCountry(id,tr)};
      bt.innerText="valider";

    }


    async function updataCountry(id,tr){

      inputs= tr.getElementsByTagName('input')
      const body = { name: inputs[0].value,
      code: inputs[1].value 
      }


    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

   var raw = JSON.stringify(body);

  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("restonsApi/country/"+id, requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));


    }

