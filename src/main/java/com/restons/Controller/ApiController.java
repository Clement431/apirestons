package com.restons.Controller;

import java.util.Optional;
import java.util.Set;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.ConstraintViolation;

import com.restons.ErrorResponse;
import com.restons.Entity.Country;
import com.restons.Repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller 
@RequestMapping(path="/restonsApi")
public class ApiController {
  
  @Autowired 
  private CountryRepository countryRepository;
  
  @GetMapping(path="/country")
  public @ResponseBody Iterable<Country> getAllCountry() {
    return countryRepository.findAll();
  }
  
  
  @GetMapping(path="/country/{id}")
  public @ResponseBody ResponseEntity<Country> getOneCountry(@PathVariable Integer id) {
    
    Optional<Country> country = countryRepository.findById(id);

    if(country.isPresent()){
      return new ResponseEntity<>(country.get(), HttpStatus.FOUND);
    }
    return new ResponseEntity<>(new Country("ERROR", "Impossible d'obtenir le pays portant l'identifiant "+id), HttpStatus.NOT_FOUND);

    
  }
  
  @PostMapping(path="/country") 
  public @ResponseBody ResponseEntity<ErrorResponse> addNewCountry (@RequestBody Country country) 
  {

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();

    Set<ConstraintViolation<Country>> constraintViolations = validator.validate(country);
      for (ConstraintViolation<Country> violation : constraintViolations) {
        return new ResponseEntity<>( new ErrorResponse(violation.getMessage(), 404), HttpStatus.BAD_REQUEST);
    }
    country.setCode(country.getCode().toUpperCase());
      countryRepository.save(country);
    return new ResponseEntity<>( new ErrorResponse("Created", 201), HttpStatus.CREATED);

  }
  
  
  @DeleteMapping(path="/country/{id}")
  public @ResponseBody ResponseEntity<ErrorResponse> removeCountry(@PathVariable Integer id)
  {
    try {
      countryRepository.deleteById(id);
    } catch (Exception  e) {
      return new ResponseEntity<>( new ErrorResponse(e.getMessage(), 400), HttpStatus.NOT_FOUND);

    }
  
    return new ResponseEntity<>( new ErrorResponse("delete ok", 200), HttpStatus.OK);
  }
  
  @PutMapping(path="/country/{id}")
  public @ResponseBody ResponseEntity<Country> putCountry(@PathVariable Integer id, @RequestBody Country country)
  {
    Optional<Country>  pCountry = countryRepository.findById(id);

    if(!pCountry.isPresent()){
      
      return new ResponseEntity<>(new Country("ERROR", "Impossible d'obtenir le pays portant l'identifiant "+id), HttpStatus.NOT_FOUND);
    }
   
    
    if (country.getName()!=null) {
      pCountry.get().setName(country.getName());

    }
    if (country.getCode()!=null) {
      pCountry.get().setCode(country.getCode().toUpperCase());
    }

    countryRepository.save(pCountry.get());
    
    return new ResponseEntity<>(pCountry.get(), HttpStatus.FOUND);
    
  }
  
  
}
