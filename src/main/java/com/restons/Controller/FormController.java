package com.restons.Controller;


import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class FormController {

    @GetMapping(value = {"/index", "/"})
    public String getIndex() {
        return "index";
    }
}
