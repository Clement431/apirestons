package com.restons;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ErrorResponse {
    
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private int status;
    private String message;


    public ErrorResponse(String message, Integer code){

        this.status = code;
        this.message= message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
