package com.restons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestonsApplication.class, args);
	}

}
