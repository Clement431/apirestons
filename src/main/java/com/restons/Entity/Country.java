package com.restons.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Country {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;

  @NotNull
  private String name;

  @NotNull
  @Size(min = 2, max = 2, message  = "La taille du code doit être egale à 2")
  private String code;

  public Country(){}

  public Country(String name, String code) {
      this.name = name;
      this.code = code;
  }

    public Integer getId() {
    return id;
  }

    public void setId(Integer id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }


}