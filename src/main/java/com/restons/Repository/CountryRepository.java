package com.restons.Repository;

//import java.util.List;

import com.restons.Entity.Country;

import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Integer> {

 // List<Country> findByLastName(String lastName);

  // Country findById(long id);
}