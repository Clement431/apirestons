
### Installation
Le serveur est sur l'adresse http://localhost:8080/  
La base de données MYsql a comme nom utilisateur root et pas de mots passe.  
Cette configuration peut être changé dans le fichier application.properties  


### Lancement
Pour lancer le projet:

Executer la commande `java -jar restons-0.0.1.jar` a l'emplacement du jar ou faire un `mvnw spring-boot-loader:run` à la racine du projet

   
### Les différentes routes de l'API:

    GET /countries => La liste de tous les pays
    GET /country/{id} => Les informations du pays portant cet id
    POST /country => Enregistre un pays
    PUT /country/{id} => Met à jour un pays portant cet id
    DELETE /country/{id} => Supprime le pays portant cet id

### Affichage
Le front est situer à l'adresse: http://localhost:8080/index

La page affiche les pays et leur codes. On peut ajouter, supprimer et update des pays. Quand on ajoute il faut refresh pour le voir dans la liste. Il a des `console.log()` à chaque appelle d'api pour voir le bon fonctionement ou pas.

### Requete curl
GET /countries:
`curl --location --request GET 'localhost:8080/restonsApi/country'`

GET /country/{id}:
`curl --location --request GET 'localhost:8080/restonsApi/country/1'`

 POST /country:
`curl --location --request POST 'localhost:8080/restonsApi/country' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "italie",
    "code": "IT"
}'`

PUT /country/{id}:
`curl --location --request PUT 'localhost:8080/restonsApi/country/8' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Mexico",
    "code": "MX"
}'`

 DELETE /country/{id}:
 `curl --location --request DELETE 'localhost:8080/restonsApi/country/1'`


